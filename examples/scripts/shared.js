function addFiles(name) {
    return [
        {
            name: `${name}.ifc`,
            getDownloadURL: () => new Promise(resolve => resolve(`examples/models/${name}.ifc`))
        },
        {
            name: `${name}.small.glb`,
            getDownloadURL: () => new Promise(resolve => resolve(`models/${name}.small.glb`))
        },
        {
            name: `${name}.small.usdz`,
            getDownloadURL: () => new Promise(resolve => resolve(`models/${name}.small.usdz`))
        },
        {
            name: `${name}.medium.glb`,
            getDownloadURL: () => new Promise(resolve => resolve(`models/${name}.medium.glb`))
        },
        {
            name: `${name}.medium.usdz`,
            getDownloadURL: () => new Promise(resolve => resolve(`models/${name}.medium.usdz`))
        },
        {
            name: `${name}.full.glb`,
            getDownloadURL: () => new Promise(resolve => resolve(`models/${name}.full.glb`))
        },
        {
            name: `${name}.full.usdz`,
            getDownloadURL: () => new Promise(resolve => resolve(`models/${name}.full.usdz`))
        },
    ];
}

function localFiles() {
    return {
        listAll: () => {
            return new Promise((resolve => {
                resolve({
                    items: [
                        ...addFiles('IFC Schependomlaan'),
                        ...addFiles('Conference Center'),
                        ...addFiles('WestRiverSide'),
                        ...addFiles('AC14-FZK-Haus')
                    ]
                });
            }));
        }
    }
}
