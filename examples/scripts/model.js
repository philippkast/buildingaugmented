var app = new Vue({
    el: '#app',
    data: {
        title: '',
        message: 'Server side processing in progress...',
        showLoader: true,
    },
    methods: {
        sizeToSmall: function() {
            modelView.modelGlb = modelView.modelSmallGlb;
            modelView.modelUsdz = modelView.modelSmallUsdz;
        },
        sizeToMedium: function() {
            modelView.modelGlb = modelView.modelMediumGlb;
            modelView.modelUsdz = modelView.modelMediumUsdz;
        },
        sizeToLarge: function() {
            modelView.modelGlb = modelView.modelLargeGlb;
            modelView.modelUsdz = modelView.modelLargeUsdz;
        },
    }
})

var modelView = new Vue({
    el: '#modelView',
    data: {
        modelGlb: '',
        modelUsdz: '',
        modelSmallGlb: '',
        modelSmallUsdz: '',
        modelMediumGlb: '',
        modelMediumUsdz: '',
        modelLargeGlb: '',
        modelLargeUsdz: '',
    },
})

function updateLoadUi() {
    if (app.title && modelView.modelSmallGlb && modelView.modelSmallUsdz) {
        app.showLoader = false;
        app.message = '';
    }
}

function getDownloadUrl(itemRef, fileFix) {
    if (itemRef.name.endsWith(`.${fileFix}.glb`)) {
        itemRef.getDownloadURL().then(function(url) {
            if (fileFix === 'full') {
                modelView.modelLargeGlb = url;
            }
            else if (fileFix === 'medium') {
                modelView.modelMediumGlb = url;
            }
            else {
                modelView.modelSmallGlb = url;
                modelView.modelGlb = url;
            }
            updateLoadUi();
          }).catch(function(error) {
            console.error(error);
          });
    }
    if (itemRef.name.endsWith(`.${fileFix}.usdz`)) {
        itemRef.getDownloadURL().then(function(url) {
            if (fileFix === 'full') {
                modelView.modelLargeUsdz = url;
            }
            else if (fileFix === 'medium') {
                modelView.modelMediumUsdz = url;
            }
            else {
                modelView.modelSmallUsdz = url;
                modelView.modelUsdz = url;
            }
            updateLoadUi();
          }).catch(function(error) {
            console.error(error);
          });
    }
}

function list() {
    var list = document.getElementById('list-example');

    const storageRef = firebase.storage();
    var user = sessionStorage.getItem('user');

    // Create a reference under which you want to list
    //var listRef = storageRef.ref(user ? `/user/${user}` : '/demo');
    var listRef;
    user ? listRef = storageRef.ref(`/user/${user}`) : listRef = localFiles();

    // Find all the prefixes and items.
    listRef.listAll().then(function(res) {
        // res.prefixes.forEach(function(folderRef) {
        //     // All the prefixes under listRef.
        //     // You may call listAll() recursively on them.
        // });
        res.items.forEach(function(itemRef) {
            // All the items under listRef.
            if (!sessionStorage.getItem('model').startsWith(itemRef.name.substring(0, itemRef.name.indexOf('.')))) {
                return;
            }
            if (itemRef.name === sessionStorage.getItem('model')) {
                app.title = itemRef.name;
            }
            getDownloadUrl(itemRef, 'small');
            getDownloadUrl(itemRef, 'medium');
            getDownloadUrl(itemRef, 'full');
            updateLoadUi();
        });
    }).catch(function(error) {
        // Uh-oh, an error occurred!
        console.error(error);
        //defaultList();
    });
}

function ready() {
    list();
}

document.addEventListener("DOMContentLoaded", ready);